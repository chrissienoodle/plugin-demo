<?php
/*
 * Plugin Name: Mon Plugin
 * Author: Christine
 * Description: mon super plugin
 * Text Domain: superplugin
 */

add_action('wp_enqueue_scripts', 'enqueue_plugin_style');
function enqueue_plugin_style(){
    wp_enqueue_style('enqueue_plugin_style',
                     plugin_dir_url(__FILE__).'/style.css'
    );
}


add_shortcode('super-shortcode', 'mon_super_shortcode');
if(!function_exists('mon_super_shortcode')){
    function mon_super_shortcode() {
        //retourne la date d'aujourdhui au form jj/mm/aaaa
        return date('d/m/Y');
    }
}

add_shortcode('user-shortcode', 'say_hello_to_user');
if (!function_exists('say_hello_to_user')){
    function say_hello_to_user(){
        //retourne 'cher nouvel ami' si utilisateur non connecté
        if(wp_get_current_user()->ID === 0){
            return 'cher nouvel ami';
        }

        //sinon retourne le nom de l'utilisateur
        return wp_get_current_user()->display_name;
    }
}

add_shortcode('dice', 'random_dice_roll');
if (!function_exists('random_dice_roll')){
    function random_dice_roll($atts){
        $atts = shortcode_atts(
            array(
                'face' => 6,
            ),
            $atts,
            'dice'
        );

        return rand(1, $atts['face']);
    }
}

add_shortcode('super-button', 'super_button_generator');
if (!function_exists('super_button_generator')){
    function super_button_generator($atts, $content){
        $atts = shortcode_atts(
            array(
                'class' => '',
                'target' => '#',
            ),
            $atts,
            'super-button'
        );

        return '<a href="'.$atts['target'].'" 
                class="'.$atts['class'].'">'.$content.'</a>';
    }
}

add_shortcode('super-color', 'alter_paragraph_color');
if (!function_exists('alter_paragraph_color')){
    function alter_paragraph_color($atts, $content){
        $atts = shortcode_atts(
          array(
              'text-color'  => 'pink',
          ),
            $atts,
            'super-color'
        );

        return '<span style="color: '.$atts['text-color'].'">'.$content.'</span>';
    }
}

add_shortcode('note', 'create_star_rating');
if (!function_exists('create_star_rating')){
    function create_star_rating($atts){
        $atts = shortcode_atts(
            array(
                'rating'=> 3,
                'img' => '/wp-content/plugins/super-plugin/imgs/star.svg'
            ),
            $atts,
            'note'
        );

        $star_rating = '<div>';

        for ($i = 0; $i<$atts['rating']; $i++) {
            $star_rating .= '<figure class="star"><img src="'.$atts['img'].'" alt="*" /></figure>';
        }

        $star_rating .= '</div>';
        return $star_rating;
    }
}

add_filter('the_title', 'modify_the_title');
if(!function_exists('modify_the_title')){
    function modify_the_title($the_title){
        if(wp_get_current_user()->ID != 0){
            $the_title = str_replace('Membre',
                                     'Supermembre',
                                     $the_title);
        }
        return $the_title;
    }
}

add_filter('comment_text', 'comment_censorship');
if(!function_exists('comment_censorship')){
    function comment_censorship($text){
        return str_replace('nul', 'génial', $text);
    }
}

add_filter('preprocess_comment', 'comment_censorship_db');
if(!function_exists('comment_censorship_db')){
    function comment_censorship_db($comment){
        $comment['comment_content'] = str_replace('moche', 'beau', $comment['comment_content']);
        return $comment;
    }
}
